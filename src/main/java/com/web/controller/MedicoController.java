package com.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.web.model.Medico;
import com.web.service.IMedicoService;

@RestController
@RequestMapping(value = { "/medicos", "/doctores" })
public class MedicoController {

	@Autowired
	private IMedicoService service;

	@GetMapping(value = { "/listar", "/getAll" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Medico>> listar() {
		List<Medico> medicos = new ArrayList<>();
		try {
			medicos = service.listar();
		} catch (Exception ex) {
			return new ResponseEntity<List<Medico>>(medicos, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Medico>>(medicos, HttpStatus.OK);
	}

	@PostMapping(value = { "/registrar","/save" }, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrar(@RequestBody Medico medico) {
		int resultado = 0;
		try {
			service.registrar1(medico);
			resultado = 1;
		} catch (Exception e) {
			resultado = 0;
			return new ResponseEntity<Integer>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}

	@PostMapping(value = {"/save2","/registrar2"}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Medico> registrar2(@RequestBody Medico medico) {
		//int resultado = 0;
		Medico m = new Medico();
		try {
			m = service.registrar2(medico);
			//resultado = 1;
		} catch (Exception e) {
			//resultado = 0;
			return new ResponseEntity<Medico>(m, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Medico>(m, HttpStatus.OK);
	}

	@PutMapping(value = { "/update",
			"/actualizar" }, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> actualizar(@RequestBody Medico medico) {
		int resultado = 0;
		try {
			service.modificar(medico);
			resultado = 1;
		} catch (Exception e) {
			resultado = 0;
			return new ResponseEntity<Integer>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}

	@DeleteMapping(value = "/eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminar(@PathVariable("id") Integer id) {
		int resultado = 0;
		try {
			service.eliminar(id);
			resultado = 1;
		} catch (Exception e) {
			resultado = 0;
			return new ResponseEntity<Integer>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}

	@GetMapping(value = { "/count", "/c", "/contar" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public long count() {
		return service.count();
	}
}
