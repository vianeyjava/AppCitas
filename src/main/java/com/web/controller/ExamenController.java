package com.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.web.model.Examen;
import com.web.service.IExamenService;

@RestController
@RequestMapping(value = { "/examenes", "Examenes" })
public class ExamenController {
	@Autowired
	private IExamenService service;

	@GetMapping(value = { "/listar", "/getAll" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Examen>> listar() {
		List<Examen> examenes = new ArrayList<>();
		try {
			examenes = service.listar();
		} catch (Exception ex) {
			return new ResponseEntity<List<Examen>>(examenes, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Examen>>(examenes, HttpStatus.OK);
	}

	@GetMapping(value = "/listar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Examen> listarId(@PathVariable("id") Integer id) {
		Examen examen = new Examen();
		try {
			examen = service.listarId(id);
		} catch (Exception ex) {
			return new ResponseEntity<Examen>(examen, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Examen>(examen, HttpStatus.OK);
	}

	@PostMapping(value = { "/registrar", "/save",
			"/s" }, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrar(@RequestBody Examen examen) {
		int resultado = 0;
		try {
			service.registrar(examen);
			resultado = 1;
		} catch (Exception e) {
			resultado = 0;
			return new ResponseEntity<Integer>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}

	@PostMapping(value = { "/registrar2",
			"/save2" }, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Examen> registrar2(@RequestBody Examen examen) {
		Examen exam = new Examen();
		try {
			exam = service.registrar2(examen);
		} catch (Exception e) {
			return new ResponseEntity<Examen>(exam, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Examen>(exam, HttpStatus.OK);
	}

	@PutMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> actualizar(@RequestBody Examen examen) {
		int resultado = 0;
		try {
			service.modificar(examen);
			resultado = 1;
		} catch (Exception e) {
			resultado = 0;
			return new ResponseEntity<Integer>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}

	@DeleteMapping(value = "/eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminar(@PathVariable Integer id) {
		int resultado = 0;
		try {
			service.eliminar(id);
			resultado = 1;
		} catch (Exception e) {
			resultado = 0;
			return new ResponseEntity<Integer>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}

	@GetMapping(value = { "/count", "/c", "/contar" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public long count() {
		return service.count();
	}
}
