package com.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppCitasApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppCitasApplication.class, args);
	}
}
