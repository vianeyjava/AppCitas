package com.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.web.dao.IExamenDAO;
import com.web.model.Examen;
import com.web.service.IExamenService;

@Service
public class ExamenServiceImpl implements IExamenService {

	@Autowired
	private IExamenDAO dao;

	@Override
	@Transactional
	public void registrar(Examen examen) {
		// TODO Auto-generated method stub
		this.dao.save(examen);
	}

	@Override
	@Transactional
	public Examen registrar2(Examen examen) {
		// TODO Auto-generated method stub
		return dao.save(examen);
	}

	@Override
	@Transactional
	public void modificar(Examen examen) {
		// TODO Auto-generated method stub
		this.dao.save(examen);
	}

	@Override
	@Transactional
	public void eliminar(int idExamen) {
		// TODO Auto-generated method stub
		this.dao.delete(idExamen);
	}

	@Override
	@Transactional(readOnly = true)
	public Examen listarId(int idExamen) {
		// TODO Auto-generated method stub
		return dao.findOne(idExamen);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Examen> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public long count() {
		// TODO Auto-generated method stub
		return dao.count();
	}

}
