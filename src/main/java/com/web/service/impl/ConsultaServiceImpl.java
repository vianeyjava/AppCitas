package com.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.web.dao.IConsultaDAO;
import com.web.model.Consulta;
import com.web.service.IConsultaService;

@Service
public class ConsultaServiceImpl implements IConsultaService {

	@Autowired
	private IConsultaDAO dao;

	@Override
	@Transactional(readOnly = true)
	public Consulta registrar(Consulta consulta) {
		// TODO Auto-generated method stub
		// Esto es java7
		/*
		 * for(DetalleConsulta det : consulta.getDetalleConsulta()) {
		 * det.setConsulta(consulta); }
		 */
		// java 8
		consulta.getDetalleConsulta().forEach(x -> x.setConsulta(consulta));
		return dao.save(consulta);
	}

	@Override
	@Transactional
	public void modificar(Consulta consulta) {
		// TODO Auto-generated method stub
		this.dao.save(consulta);
	}

	@Override
	@Transactional
	public void eliminar(int idConsulta) {
		// TODO Auto-generated method stub
		this.dao.delete(idConsulta);
	}

	@Override
	@Transactional(readOnly = true)
	public Consulta listarId(int idConsulta) {
		// TODO Auto-generated method stub
		return dao.findOne(idConsulta);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Consulta> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public long count() {
		// TODO Auto-generated method stub
		return dao.count();
	}

}
