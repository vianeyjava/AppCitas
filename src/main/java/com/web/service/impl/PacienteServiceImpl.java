package com.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.web.dao.IPacienteDAO;
import com.web.model.Paciente;
import com.web.service.IPacienteService;

@Service
public class PacienteServiceImpl implements IPacienteService {

	@Autowired
	private IPacienteDAO dao;

	@Override
	@Transactional
	public Paciente registrar(Paciente paciente) {
		// TODO Auto-generated method stub
		return dao.save(paciente);
	}

	@Override
	public void registrar2(Paciente paciente) {
		// TODO Auto-generated method stub
		this.dao.save(paciente);
	}

	@Override
	@Transactional
	public void modificar(Paciente paciente) {
		// TODO Auto-generated method stub
		this.dao.save(paciente);
	}

	@Override
	@Transactional
	public void eliminar(int idPaciente) {
		// TODO Auto-generated method stub
		this.dao.delete(idPaciente);
	}

	@Override
	@Transactional(readOnly = true)
	public Paciente listarId(int idPaciente) {
		// TODO Auto-generated method stub
		return dao.findOne(idPaciente);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Paciente> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public long count() {
		// TODO Auto-generated method stub
		return dao.count();
	}

}
