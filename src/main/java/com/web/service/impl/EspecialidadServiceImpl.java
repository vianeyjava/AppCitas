package com.web.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.dao.IEspecialidadDAO;
import com.web.model.Especialidad;
import com.web.service.IEspecialidadService;

@Service
public class EspecialidadServiceImpl implements IEspecialidadService {

	@Autowired
	private IEspecialidadDAO dao;

	@Override
	@Transactional
	public void registrar(Especialidad especialidad) {
		// TODO Auto-generated method stub
		this.dao.save(especialidad);
	}

	@Override
	@Transactional
	public void modificar(Especialidad especialidad) {
		// TODO Auto-generated method stub
		this.dao.save(especialidad);
	}

	@Override
	@Transactional
	public void eliminar(int idEspecialidad) {
		// TODO Auto-generated method stub
		this.dao.delete(idEspecialidad);
	}

	@Override
	@Transactional(readOnly = true)
	public Especialidad listarId(int idEspecialidad) {
		// TODO Auto-generated method stub
		return dao.findOne(idEspecialidad);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Especialidad> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public long count() {
		// TODO Auto-generated method stub
		return dao.count();
	}

}
