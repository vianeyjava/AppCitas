package com.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.web.dao.IMedicoDAO;
import com.web.model.Medico;
import com.web.service.IMedicoService;

@Service
public class MedicoServiceImpl implements IMedicoService {

	@Autowired
	private IMedicoDAO dao;

	@Override
	@Transactional
	public void registrar1(Medico medico) {
		// TODO Auto-generated method stub
		this.dao.save(medico);
	}

	@Override
	@Transactional
	public Medico registrar2(Medico medico) {
		return dao.save(medico);
	}
	@Override
	@Transactional
	public void modificar(Medico medico) {
		// TODO Auto-generated method stub
		this.dao.save(medico);
	}

	@Override
	@Transactional
	public void eliminar(int idMedico) {
		// TODO Auto-generated method stub
		this.dao.delete(idMedico);
	}

	@Override
	@Transactional(readOnly = true)
	public Medico listarId(int idMedico) {
		// TODO Auto-generated method stub
		return dao.findOne(idMedico);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Medico> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public long count() {
		// TODO Auto-generated method stub
		return dao.count();
	}

}
