package com.web.service;

import java.util.List;

import com.web.model.Medico;

public interface IMedicoService {

	public void registrar1(Medico medico);
	
	public Medico registrar2(Medico medico);

	public void modificar(Medico medico);

	public void eliminar(int idMedico);

	public Medico listarId(int idMedico);

	public List<Medico> listar();
	
	public long count();
}
