package com.web.service;

import java.util.List;

import com.web.model.Examen;

public interface IExamenService {

	public void registrar(Examen examen);
	
	public Examen registrar2(Examen examen);

	public void modificar(Examen examen);

	public void eliminar(int idExamen);

	public Examen listarId(int idExamen);

	public List<Examen> listar();
	
	public long count();
}
