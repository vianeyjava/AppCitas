package com.web.service;

import java.util.List;

import com.web.model.Paciente;

public interface IPacienteService {

	public Paciente registrar(Paciente paciente);
	
	public void registrar2(Paciente paciente);

	public void modificar(Paciente paciente);

	public void eliminar(int idPaciente);

	public Paciente listarId(int idPaciente);

	public List<Paciente> listar();
	
	public long count();
}
