package com.web.service;

import java.util.List;

import com.web.model.Consulta;

public interface IConsultaService {

	public Consulta registrar(Consulta consulta);

	public void modificar(Consulta consulta);

	public void eliminar(int idConsulta);

	public Consulta listarId(int idConsulta);

	public List<Consulta> listar();
	
	public long count();
}
