package com.web.service;

import java.util.List;

import com.web.model.Especialidad;

public interface IEspecialidadService {

	public void registrar(Especialidad especialidad);

	public void modificar(Especialidad especialidad);

	public void eliminar(int idEspecialidad);

	public Especialidad listarId(int idEspecialidad);

	public List<Especialidad> listar();
	
	public long count();
}
